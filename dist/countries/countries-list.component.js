angular.
  module('countries-list').
  component('countries', {
    templateUrl: 'dist/countries/countries-list.template.html',
    controller: countriesListController
  });

function countriesListController($scope, $http) {
    let totalDevices = 0;
    let totalChannels = 0;
    let payload = [];
    $http.get("data.json").then(function(res){
      for(var key in res.data){
        totalDevices += res.data[key].Devices;
        totalChannels += res.data[key].Channels;
        payload.push({
          name: key || 'Unknown',
          Channels: res.data[key].Channels,
          Devices: res.data[key].Devices,
          channelWidth: 20 + (res.data[key].Channels) + 'px',
          deviceWidth: 20 + (res.data[key].Devices) + 'px'
        });
      }
      $scope.totalDevices = totalDevices;
      $scope.totalChannels = totalChannels;
      $scope.countries = payload;
    });
  }
